use std::sync::Arc;
use std::thread;
use std::time::Duration;

use esp_idf_hal::delay::FreeRtos;
use esp_idf_hal::gpio::*;
use esp_idf_hal::ledc::config::TimerConfig;
use esp_idf_hal::ledc::*;
use esp_idf_hal::peripherals::Peripherals;
use esp_idf_hal::prelude::*;
use esp_idf_sys as _; // If using the `binstart` feature of `esp-idf-sys`, always keep this module imported
use log::*;

fn main() {
    // It is necessary to call this function once. Otherwise some patches to the runtime
    // implemented by esp-idf-sys might not link properly. See https://github.com/esp-rs/esp-idf-template/issues/71
    esp_idf_sys::link_patches();
    // Bind the log crate to the ESP Logging facilities
    esp_idf_svc::log::EspLogger::initialize_default();

    let peripherals = Peripherals::take().unwrap();
    let config = config::TimerConfig::new().frequency(255.kHz().into());
    let timer = Arc::new(LedcTimerDriver::new(peripherals.ledc.timer0, &config).unwrap());

    let mut red_pwm = LedcDriver::new(
        peripherals.ledc.channel0,
        timer.clone(),
        peripherals.pins.gpio40,
    )
    .unwrap();

    let mut green_pwm = LedcDriver::new(
        peripherals.ledc.channel1,
        timer.clone(),
        peripherals.pins.gpio39,
    )
    .unwrap();

    let mut blue_pwm = LedcDriver::new(
        peripherals.ledc.channel2,
        timer.clone(),
        peripherals.pins.gpio38,
    )
    .unwrap();

    info!("Starting blinky...");

    loop {
        for i in 0..256 {
            let colors = wheel(i);

            let red = 256 - colors[0];
            let green = 256 - colors[1];
            let blue = 256 - colors[2];
            println!("red: {:?}, green: {:?}, blue: {:?}", red, green, blue);
            red_pwm.set_duty(red).unwrap();
            green_pwm.set_duty(green).unwrap();
            blue_pwm.set_duty(blue).unwrap();
            thread::sleep(Duration::from_millis(15));
        }
    }
}

fn wheel(pos: u32) -> [u32; 3] {
    let mut wheel_pos = pos % 256;
    let mut to_return: [u32; 3];
    println!("{}", wheel_pos);
    if wheel_pos < 85 {
        let red = 256 - wheel_pos * 3;
        let green = wheel_pos * 3;
        let blue = 0;
        to_return = [red, green, blue]
    } else if wheel_pos < 170 {
        wheel_pos -= 85;
        let red = 0;
        let blue = 256 - wheel_pos * 3;
        let green = wheel_pos * 3;

        to_return = [red, green, blue]
    } else {
        wheel_pos -= 170;

        let red = wheel_pos * 3;
        let green = 0;
        let blue = 256 - wheel_pos * 3;

        to_return = [red, green, blue]
    }

    to_return
}
